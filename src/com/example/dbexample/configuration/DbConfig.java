package com.example.dbexample.configuration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConfig {
    private final static String DB_CONNECTION = "jdbc:mysql://localhost:4589/databasedemon0";
    private final static String USERNAME = "root";
    private final static String PASSWORD = "root";

    public static Connection getDbConnection(){
        //loading the driver class for database
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch(ClassNotFoundException e) {
            System.out.println("Error while loading the driver class");
        }

        //connecting to the database inside the DBMS
        Connection connection=null;
        try {
            connection = DriverManager.getConnection(DB_CONNECTION,USERNAME,PASSWORD);
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }
}
