package com.example.dbexample;

import com.example.dbexample.configuration.DbConfig;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DbDemoMain {

    private final static String SELECT_STUDENT_QUERY="select * from student";
//checking pull request
    public static void main(String[] args) {

        try {
            Statement statement1 = DbConfig.getDbConnection().createStatement();
            //execute the sql query and sets the value of the query to the resultset object
            ResultSet resultSet = statement.executeQuery(SELECT_STUDENT_QUERY);

            while(resultSet.next()){

                System.out.println(resultSet.getInt(1)+" "+resultSet.getString(2)+" "+resultSet.getString(3));

            }

        } catch(SQLException e) {
            System.out.println(e);
            System.out.println("cannot execute the statement");
        }
    }
}
