package com.example.dbexample;


import com.example.dbexample.configuration.DbConfig;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class InsertDbDemoMain {
    private static final String INSERT_QUERY="insert into student(id,name,address) values(?,?,?)";

    public static void main(String[] args) {

        try {
            PreparedStatement preparedStatement = DbConfig.getDbConnection().prepareStatement(INSERT_QUERY);
            preparedStatement.setInt(1,23);
            preparedStatement.setString(2,"Suresh");
            preparedStatement.setString(3,"Chabahil");

            int rowsUpdated = preparedStatement.executeUpdate();
            System.out.println("Rows updated after changes in query "+rowsUpdated);

        } catch(SQLException e) {
            e.printStackTrace();
        }
    }
}
